﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lu8890.UtilityLibraries.DataStructure
{
    public class Node<T>
    {
        public T NodeValue { get; set; }
        public Node<T> NextNode { get; set; }

        public Node(T value)
        {
            this.NodeValue = value;
            this.NextNode = null;
        }

        public Node()
        {
        }
    }
}
