﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lu8890.UtilityLibraries.DataStructure
{
    public class Stack<T>: LinkedList<T>
    {
        public Stack()
        {

        }

        public Stack(T[] inputs)
        {
            foreach (var input in inputs)
            {
                Push(new Node<T>(input));
            }
        }
        public void Push(Node<T> newNode)
        {
            base.AddANode(newNode);
        }

        public Node<T> Pop()
        {
            return base.GetRemoveNodeByPosition(base.Length);
        }
    }
}
