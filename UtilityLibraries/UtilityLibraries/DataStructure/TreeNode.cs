﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lu8890.UtilityLibraries.DataStructure
{
    public class TreeNode: Node<int>
    {
        public TreeNode Left { get; set; }
        public TreeNode Right { get; set; }

        public TreeNode(int value):base(value)
        {
            base.NodeValue = value;
            this.Left = null;
            this.Right = null;
        }

        public TreeNode()
        {
        }
    }
}
