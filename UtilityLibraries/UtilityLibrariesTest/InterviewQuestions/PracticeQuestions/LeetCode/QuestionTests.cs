﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using lu8890.UtilityLibraries.InterviewQuestions.PracticeQuestions.LeetCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lu8890.UtilityLibraries.InterviewQuestions.PracticeQuestions.LeetCode.Tests
{
    [TestClass()]
    public class QuestionTests
    {
        [TestMethod()]
        public void ReverseTest()
        {
            Question q = new Question();
            Assert.AreEqual(q.Reverse(1),1);
            Assert.AreEqual(q.Reverse(-1), -1);
            Assert.AreEqual(q.Reverse(12),21);
            Assert.AreEqual(q.Reverse(123), 321);
            Assert.AreEqual(q.Reverse(-123), -321);
            Assert.AreEqual(q.Reverse(120), 21);
            Assert.AreEqual(q.Reverse(1534236469), 0);
        }

        [TestMethod()]
        public void IsPalindromeTest()
        {
            Question q = new Question();
            Assert.AreEqual(q.IsPalindrome(1), true);
            Assert.AreEqual(q.IsPalindrome(-1), false);
            Assert.AreEqual(q.IsPalindrome(12), false);
            Assert.AreEqual(q.IsPalindrome(121), true);
            Assert.AreEqual(q.IsPalindrome(-121), false);
            Assert.AreEqual(q.IsPalindrome(10), false);
            Assert.AreEqual(q.IsPalindrome(1234321), true);
            Assert.AreEqual(q.IsPalindrome(12344321), true);
        }

        [TestMethod()]
        public void RomanToIntTest()
        {
            Question q = new Question();
            Assert.AreEqual(q.RomanToInt("I"), 1);
            Assert.AreEqual(q.RomanToInt("i"), 1);
            Assert.AreEqual(q.RomanToInt("III"), 3);
            Assert.AreEqual(q.RomanToInt("IV"), 4);
            Assert.AreEqual(q.RomanToInt("IX"), 9);
            Assert.AreEqual(q.RomanToInt("LVIII"), 58);
            Assert.AreEqual(q.RomanToInt("MCMXCIV"), 1994);
        }

        [TestMethod()]
        public void LongestCommonPrefixTest()
        {
            Question q = new Question();
            var testCase = new string[] {"TotalPackage"};
            Assert.AreEqual("TotalPackage", q.LongestCommonPrefix(testCase));
            testCase = new string[]{ "flower", "flow", "flight" };
            Assert.AreEqual("fl", q.LongestCommonPrefix(testCase));
            testCase = new string[] { "dog", "racecar", "car" };
            Assert.AreEqual("", q.LongestCommonPrefix(testCase));
        }

        [TestMethod()]
        public void IsValidTest()
        {
            Question q = new Question();
            Assert.AreEqual(true, q.IsValid(""));
            Assert.AreEqual(true, q.IsValid("()"));
            Assert.AreEqual(true, q.IsValid("()[]{}"));
            Assert.AreEqual(false, q.IsValid("(]"));
            Assert.AreEqual(false, q.IsValid("([)]"));
            Assert.AreEqual(true, q.IsValid("{[]}"));
            Assert.AreEqual(false, q.IsValid("(("));
        }
    }
}