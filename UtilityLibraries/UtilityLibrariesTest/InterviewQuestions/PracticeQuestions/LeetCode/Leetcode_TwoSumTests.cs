﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using lu8890.UtilityLibraries.InterviewQuestions.PracticeQuestions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lu8890.UtilityLibraries.InterviewQuestions.PracticeQuestions.LeetCode.Tests
{
    [TestClass()]
    public partial class QuestionTest
    {
        [TestMethod()]
        public void TwoSumTest()
        {
            var test = new Question();;
            Assert.AreEqual(null, test.TwoSum(new int[] { }, 10 ));
            Assert.AreEqual(null, test.TwoSum(new int[] { 1}, 10));
            Assert.AreEqual(null, test.TwoSum(new int[] { 2, 3}, 10));
            Assert.AreEqual(string.Join(", ",new int[]{0,1}).Trim(), 
                string.Join(", ", test.TwoSum(new int[] {5, 5 }, 10)).Trim());
            Assert.AreEqual(string.Join(", ", new int[]{0,1}).Trim(),
                string.Join(", ", test.TwoSum(new int[] {2,7,11,15 }, 9)).Trim());

            Assert.AreEqual(string.Join(", ", new int[] { 0, 3 }).Trim(),
                string.Join(", ", test.TwoSum(new int[] { 2, 11, 15, 7 }, 9)).Trim());

            Assert.AreEqual(string.Join(", ", new int[] { 0, 3 }).Trim(),
                string.Join(", ", test.TwoSum(new int[] { 7, 11, 15, 2, 7 }, 9)).Trim());

            Assert.AreEqual(string.Join(", ", new int[] { 1, 2 }).Trim(),
                string.Join(", ", test.TwoSum(new int[] {3,2,4}, 6)).Trim());
        }
    }
}