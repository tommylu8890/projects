﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using lu8890.UtilityLibraries.DataStructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;

namespace lu8890.UtilityLibraries.DataStructure.Tests
{
    [TestClass()]
    public class TreeTests
    {
        [TestMethod()]
        public void TreeTest()
        {
            var testInput1 = new[] {1, 2, 5, 22, -3, 5};
            var tree1 = new Tree(testInput1);
            tree1.PrintTreePreOrder(tree1.Root);
            Assert.AreEqual(tree1.Output.ToString().Trim(), "5 1 -3 2 22 5");
            Console.WriteLine("------------------------------------");
            tree1.Output = new StringBuilder();
            tree1.PrintTreeInOrder(tree1.Root);
            Assert.AreEqual(tree1.Output.ToString().Trim(), "1 -3 2 5 22 5");
            Console.WriteLine("------------------------------------");
            tree1.Output = new StringBuilder();
            tree1.PrintTreePostOrder(tree1.Root);
            Assert.AreEqual(tree1.Output.ToString().Trim(), "1 -3 2 22 5 5");
        }
    }
}