﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using lu8890.UtilityLibraries.DataStructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lu8890.UtilityLibraries.DataStructure.Tests
{
    [TestClass()]
    public class StackTests
    {
        [TestMethod()]
        public void PushTest()
        {
            var intStack = new lu8890.UtilityLibraries.DataStructure.Stack<int>();
            intStack.Push(new Node<int>(0));
            Assert.AreEqual(intStack.Length, 1);
            var outNode = intStack.Pop();
            Assert.AreEqual(outNode.NodeValue, 0);
            Assert.AreEqual(intStack.Length, 0);

            intStack.Push(new Node<int>(0));
            intStack.Push(new Node<int>(1));
            Assert.AreEqual(intStack.Length, 2);
            outNode = intStack.Pop();
            Assert.AreEqual(outNode.NodeValue, 1);
            Assert.AreEqual(intStack.Length, 1);
            outNode = intStack.Pop();
            Assert.AreEqual(outNode.NodeValue, 0);
            Assert.AreEqual(intStack.Length, 0);

            intStack.Push(new Node<int>(0));
            intStack.Push(new Node<int>(1));
            intStack.Push(new Node<int>(2));
            intStack.Push(new Node<int>(3));
            intStack.Push(new Node<int>(4));
            intStack.Push(new Node<int>(5));
            Assert.AreEqual(intStack.Length, 6);
            outNode = intStack.Pop();
            Assert.AreEqual(outNode.NodeValue, 5);
            Assert.AreEqual(intStack.Length, 5);
        }
    }
}