﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CompanyLocationWebApi.Controllers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CompanyLocationWebApi.Tests.Controllers
{
    [TestClass]
    public class CompanyLocationWebApiTest
    {
        [TestMethod]
        public void TestGetCompanyListByNameCityCountryApi()
        {
            var controller = new CompanyNameApiController();
            var result = controller.GetCompanyListByNameCityCountry("walmart", "lynnwood", "united states");

            Assert.AreEqual(result["Count"].Value<int>(), 60, "[Test Failure]  Expected result count: 60, Actual result count: " + result["Count"]);
        }

        [TestMethod]
        public void TestGetCompanyListByNameCityCountryApiWithEmptyResult()
        {
            var controller = new CompanyNameApiController();
            var result = controller.GetCompanyListByNameCityCountry("lalaLand", "lynnwood", "united states");

            Assert.AreEqual(result["Count"].Value<int>(), 0, "[Test Failure]  Expected result count: 0, Actual result count: " + result["Count"]);
        }

        [TestMethod]
        public void TestGetCompanyListByNameCityCountryApiWithEmptyResultOnUnicodeChar()
        {
            var controller = new CompanyNameApiController();
            var result = controller.GetCompanyListByNameCityCountry("大統華", "lynnwood", "united states");

            Assert.AreEqual(result["Count"].Value<int>(), 0, "[Test Failure]  Expected result count: 0, Actual result count: " + result["Count"]);
        }



        [TestMethod]
        public void TestGetCompanyListByNameCityApiWithEmptyResult()
        {
            var controller = new CompanyNameApiController();
            var result = controller.GetCompanyListByNameCity("lalaLand", "lynnwood");

            Assert.AreEqual(result["Count"].Value<int>(), 0, "[Test Failure]  Expected result count: 0, Actual result count: " + result["Count"]);
        }

        [TestMethod]
        public void TestGetCompanyListByNameCityApiWithEmptyResultOnUnicodeChar()
        {
            var controller = new CompanyNameApiController();
            var result = controller.GetCompanyListByNameCity("大統華", "lynnwood");

            Assert.AreEqual(result["Count"].Value<int>(), 0, "[Test Failure]  Expected result count: 0, Actual result count: " + result["Count"]);
        }

        [TestMethod]
        public void TestGetCompanyListByNameCityApiWithNonExistCityEmptyResult()
        {
            var controller = new CompanyNameApiController();
            var result = controller.GetCompanyListByNameCity("lalaLand", "BuBuCity");

            Assert.AreEqual(result["Count"].Value<int>(), 0, "[Test Failure]  Expected result count: 0, Actual result count: " + result["Count"]);
        }



        //[TestMethod] disable for now until performance can be improved
        public void TestGetCompanyListByNameCountryApiWithEmptyResult()
        {
            var controller = new CompanyNameApiController();
            var result = controller.GetCompanyListByNameCountry("lalaLand", "United States");

            Assert.AreEqual(result["Count"].Value<int>(), 0, "[Test Failure]  Expected result count: 0, Actual result count: " + result["Count"]);
        }

        //[TestMethod] disable for now until performance can be improved
        public void TestGetCompanyListByNameCountryApiWithEmptyResultOnUnicodeChar()
        {
            var controller = new CompanyNameApiController();
            var result = controller.GetCompanyListByNameCountry("大統華", "United States");

            Assert.AreEqual(result["Count"].Value<int>(), 0, "[Test Failure]  Expected result count: 0, Actual result count: " + result["Count"]);
        }

        [TestMethod]
        public void TestGetCompanyListByNameCountryApiWithNonExistCityEmptyResult()
        {
            var controller = new CompanyNameApiController();
            var result = controller.GetCompanyListByNameCountry("walmart", "BuBuCountry");

            Assert.AreEqual(result["Count"].Value<int>(), 0, "[Test Failure]  Expected result count: 0, Actual result count: " + result["Count"]);
        }
    }
}
