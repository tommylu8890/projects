﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="Demo.aspx.cs" Inherits="CompanyLocationWebApi.Demo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Demo: TopCoder Challenges - Company Location Api</title>
    <style type="text/css">
        /*.auto-style1 {
            width: 25%;
            height: 231px;
        }
        .auto-style2 {
            width: 80%;
            height: 231px;
        }
        .auto-style3 {
            width: 1146px;
        }*/
        .auto-style1 {
            width: 63px;
        }
        .auto-style2 {
            width: 666px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div style="text-align: center; font-size: large">
                <asp:Label ID="Label_TopHeader" runat="server" Text="Demo: TopCoder Challenges - Company Location Api"></asp:Label>
            </div>
            <div>
                <table class="auto-style3">
                    <tr>
                        <td style="vert-align: top">
                            <asp:Panel ID="Panel1" runat="server" Height="205px" Width="358px">
                                <div></div>
                                <div style="vertical-align: middle">
                                    Enter Company Name:  <asp:TextBox ID="TextBox_CompanyNameInput" runat="server"></asp:TextBox>
                                </div>
                                <div style="vertical-align: middle; padding-top: 15px">
                                    Country: <asp:DropDownList ID="DropDownList_CountryList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList_CountryList_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                                <div style="vertical-align: middle; padding-top: 15px">
                                    City:  <asp:DropDownList ID="DropDownList_CityList" runat="server" AutoPostBack="True"></asp:DropDownList>
                                </div>
                                <div style="vertical-align: middle; padding-top: 15px">
                                    <asp:Button ID="Button_Execute" runat="server" Text="Run Search" OnClick="Button_Execute_Click" />
                                </div>
                                <div></div>
                            </asp:Panel>
                        </td>
                        <td runat="server" id="outputPanel" style="padding-left: 10px; vertical-align: text-top" class="auto-style2">
                            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                            <br/><br/>
                            <asp:GridView ID="GridView1" runat="server" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black">
                                <FooterStyle BackColor="#CCCCCC" />
                                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" />
                                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#808080" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#383838" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                
            </div>
        </div>
    </form>
</body>
</html>
