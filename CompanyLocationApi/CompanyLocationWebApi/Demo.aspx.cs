﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using CompanyLocationWebApi.Models;
using CompanyLocationWebApi.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace CompanyLocationWebApi
{
    public partial class Demo : System.Web.UI.Page
    {
        private readonly string _dataJson = AppDomain.CurrentDomain.BaseDirectory + @"\App_data\cities.json";
        private List<City> Jobjects { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var content = LoadCityGeoData();
            Jobjects = JsonConvert.DeserializeObject<List<City>>(content);

            LoadCountryDropdownListContent();
            LoadCityDropdownListContent();
        }

        private void LoadCountryDropdownListContent()
        {
            //var countryList = jobjects.Distinct().SelectMany(x => x.CountryName).Distinct();  TODO: research why output returns single char list instead of string list
            ListItem temp;
            this.DropDownList_CountryList.Items.Add(new ListItem("Pleae Select a Value", "Default"));
            foreach (var city in Jobjects.OrderBy(x=>x.CountryName))
            {
               temp = new ListItem(city.CountryName, city.CountryName);
                if(!this.DropDownList_CountryList.Items.Contains(temp))
                    this.DropDownList_CountryList.Items.Add(temp);
            }
        }

        private void LoadCityDropdownListContent(string country = null)
        {
            ListItem temp;          
            List<City> targetCities;

            if (!string.IsNullOrWhiteSpace(country))
            {
                DropDownList_CityList.Items.Clear();
                targetCities = Jobjects.FindAll(x =>
                    string.Compare(x.CountryName, country, StringComparison.OrdinalIgnoreCase) == 0).ToList();
            }
            else
            {
                targetCities = Jobjects;
            }

            this.DropDownList_CityList.Items.Add(new ListItem("Pleae Select a Value", "Default"));

            foreach (var city in targetCities.OrderBy(x => x.CityNameAscii))
            {
                    temp = new ListItem(city.CityNameAscii, city.CityNameAscii);
                    if (!this.DropDownList_CityList.Items.Contains(temp))
                        this.DropDownList_CityList.Items.Add(temp);            
            }
        }

        private string LoadCityGeoData()
        {
            string jsonRawContent;
            using (var sr = new StreamReader(_dataJson))
            {
                jsonRawContent = sr.ReadToEnd();
            }

            return jsonRawContent;
        }

        protected void DropDownList_CountryList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList_CountryList.SelectedItem.Value.Equals("Default", StringComparison.OrdinalIgnoreCase))
            {
                DropDownList_CityList.SelectedIndex = 0;
            }
            else
            {
                LoadCityDropdownListContent(DropDownList_CountryList.SelectedItem.Value);
            }
        }

        protected void Button_Execute_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TextBox_CompanyNameInput.Text))
            {
                //throw new Exception("Company name can not be empty");
            }
            else
            {
                var urlObj = HttpContext.Current.Request.Url;
                var urlScheme = urlObj.Scheme;
                var urlAuthorith = urlObj.Authority;
                const string apiPathBase = @"/api/CompanyNameApi/";
                var webApiCommandBase = urlScheme + "://" + urlAuthorith + apiPathBase;
                var companyNameApiUrl = string.Empty;

                // call GetCompanyListByNameCityCountry
                if ((DropDownList_CountryList.SelectedIndex != 0) && (DropDownList_CityList.SelectedIndex != 0))
                    companyNameApiUrl = webApiCommandBase + TextBox_CompanyNameInput.Text + "/" +
                                        DropDownList_CityList.SelectedValue + "/" +
                                        DropDownList_CountryList.SelectedValue;

                // call GetCompanyListByNameCity
                if ((DropDownList_CountryList.SelectedIndex == 0) && (DropDownList_CityList.SelectedIndex != 0))
                    companyNameApiUrl = webApiCommandBase + TextBox_CompanyNameInput.Text + "/city/" +
                                        DropDownList_CityList.SelectedValue;

                // call GetCompanyListByNameCountry
                if ((DropDownList_CountryList.SelectedIndex != 0) && (DropDownList_CityList.SelectedIndex == 0))
                    companyNameApiUrl = webApiCommandBase + TextBox_CompanyNameInput.Text + "/country/" +
                                        DropDownList_CountryList.SelectedValue;

                // call GetCompanyListByName
                if ((DropDownList_CountryList.SelectedIndex == 0) && (DropDownList_CityList.SelectedIndex == 0))
                    companyNameApiUrl = webApiCommandBase + TextBox_CompanyNameInput.Text;


                var response = Utilities.GetHttpResponse(companyNameApiUrl,
                    null,
                    HttpApiMethods.GET,
                    string.Empty,
                    HttpcontentTypes.TextHtml,
                    true);

                var resultsCol = JsonConvert.DeserializeObject<Result>(response.ReturnMessage);
                var companyCount = resultsCol.Count;
                Label1.Text = "Total Count: " + companyCount;

                if (companyCount > 0)
                {
                    var companyCol = resultsCol.Companies;
                    GridView1.DataSource = companyCol;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }
            }
        }
    }
}