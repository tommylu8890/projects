﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CompanyLocationWebApi.Models;
using CompanyLocationWebApi.Common;

namespace CompanyLocationWebApi.Controllers
{
    public class CompanyNameApiController : ApiController
    {
        private readonly string _dataJson = AppDomain.CurrentDomain.BaseDirectory + @"\App_data\cities.json";

        [Route("api/CompanyNameApi/{companyName}")]
        public JObject GetCompanyListByName(string companyName)
        {
            var content = LoadCityGeoData();
            var jobject = JsonConvert.DeserializeObject<List<City>>(content);

            return GetPlaceInfoByGooglePlaceApi(jobject, companyName);
        }

        [Route("api/CompanyNameApi/{companyName}/country/{country}")]
        public JObject GetCompanyListByNameCountry(string companyName, string country)
        {
            var content = LoadCityGeoData();
            var jobject = JsonConvert.DeserializeObject<List<City>>(content);
            var targetCol = jobject.FindAll(x =>
                (string.Compare(x.CountryName, country, StringComparison.InvariantCultureIgnoreCase) == 0)).ToList();

            return GetPlaceInfoByGooglePlaceApi(targetCol, companyName);
        }

        [Route("api/CompanyNameApi/{companyName}/city/{city}")]
        public JObject GetCompanyListByNameCity(string companyName, string city)
        {
            var content = LoadCityGeoData();
            var jobject = JsonConvert.DeserializeObject<List<City>>(content);
            var targetCol = jobject.FindAll(x =>
                (string.Compare(x.CityNameAscii, city, StringComparison.InvariantCultureIgnoreCase) == 0)).ToList();

            return GetPlaceInfoByGooglePlaceApi(targetCol, companyName);
        }

        [Route("api/CompanyNameApi/{companyName}/{city}/{country}")]
        public JObject GetCompanyListByNameCityCountry(string companyName, string city, string country)
        {
            var content = LoadCityGeoData();
            var jobjects = JsonConvert.DeserializeObject<List<City>>(content);
            var targetCol = jobjects.FindAll(x => (string.Compare(x.CountryName, country, StringComparison.OrdinalIgnoreCase) == 0) 
                                        && (string.Compare(x.CityNameAscii, city, StringComparison.InvariantCultureIgnoreCase) == 0))
                                    .ToList();

            return GetPlaceInfoByGooglePlaceApi(targetCol, companyName);
        }

        private JObject GetPlaceInfoByGooglePlaceApi(List<City> cityCol, string companyName)
        {
            GooglePlaceApi googleApi = new GooglePlaceApi(cityCol, companyName);
            googleApi.ProcessCities(companyName);

            return  googleApi.RetrieveCompanyInfoInJson();
        }

        private string LoadCityGeoData()
        {
            string jsonRawContent;
            using (var sr = new StreamReader(_dataJson))
            {
                jsonRawContent = sr.ReadToEnd();
            }

            return jsonRawContent;
        }

    }
}
