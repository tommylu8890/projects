﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CompanyLocationWebApi.Models;
using CompanyLocationWebApi.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net;
using System.Text;
using Antlr.Runtime;

namespace CompanyLocationWebApi.Common
{
    public class GooglePlaceApi
    {
        private string GooglePlaceApiCommandBase { get; set; }
        private string GooglePlaceApiCommandBaseNearby { get; set; }
        private string GoogleApiKey { get; set; }
        internal List<City> TargetCities { get; set; }
        private string CompanyName { get; set; }
        private string SearchRadiusInMeter { get; set; }
        private Dictionary<string, CompanyLocationOutput> _companyCol { get; set; }
        private readonly int _apicallReAttemptCount = 10;
        private readonly int _apicallReAttemptTimeoutInMs = 500;
        private int _repeatCounter = 0;
        private int _repeatCounter2 = 0;
        private StringBuilder _errorBuilder = new StringBuilder();


        internal GooglePlaceApi(List<City> targetCities, string companyName)
        {
            var webconfig = new WebconfigObj();

            GooglePlaceApiCommandBase = (webconfig.HasKey("GooglePlaceApiCommandBase"))
                ? webconfig.GetValue("GooglePlaceApiCommandBase") 
                : throw new ArgumentException("[Fatal Error] GooglePlaceApiCommandBase key is not found");

            GooglePlaceApiCommandBaseNearby = (webconfig.HasKey("GooglePlaceApiCommandBaseNearby"))
                ? webconfig.GetValue("GooglePlaceApiCommandBaseNearby")
                : throw new ArgumentException("[Fatal Error] GooglePlaceApiCommandBaseNearby key is not found");

            GoogleApiKey = (webconfig.HasKey("GoogleApiKey"))
                ? webconfig.GetValue("GoogleApiKey") 
                : throw new ArgumentException("[Fatal Error] GoogleApiKey key is not found");

            TargetCities = targetCities ?? throw new ArgumentNullException(nameof(targetCities), "Parameter targetCities can not be null");

            CompanyName = (!string.IsNullOrWhiteSpace(companyName))
                ? companyName
                : throw new ArgumentNullException(nameof(companyName), "Parameter companyName can not be null");

            SearchRadiusInMeter = (webconfig.HasKey("GooglePlaceSearchRadiusInMeter"))
                ? webconfig.GetValue("GooglePlaceSearchRadiusInMeter")
                : "1000";

            _companyCol = new Dictionary<string, CompanyLocationOutput>();
        }

       

        internal void ProcessCities(string searchKeyword)
        {
            foreach (var city in TargetCities)
            {
                //check Nearby API first to see if it returns no content
                var nearbyQuery = "location=" + city.GeoLat + "," + city.GeoIng + "&radius=" +
                                  SearchRadiusInMeter + "&keyword=" + CompanyName + "&key=" + GoogleApiKey;
                var googleApiNearbyCommand = GooglePlaceApiCommandBaseNearby + nearbyQuery;
                var response = ExecuteGooglePlaceApi(googleApiNearbyCommand);

                if ((response != null) && (!response.ResponseHttpStatusCode.Equals(HttpStatusCode.NoContent)))
                {
                    var apiQuery = CompanyName + "&location=" + city.GeoLat + "," + city.GeoIng + "&radius=" +
                                   SearchRadiusInMeter + "&key=" + GoogleApiKey;
                    var googleApiCommand = GooglePlaceApiCommandBase + apiQuery;
                    response = ExecuteGooglePlaceApi(googleApiCommand);
                    if (response == null)
                        throw new Exception("[Fatal Error] Failed to execute Google Place Api: " + googleApiCommand);

                    if (!response.ResponseHttpStatusCode.Equals(HttpStatusCode.NoContent))
                        ProcessGoogleApiResults(response.ReturnMessage, googleApiCommand);
                }
            }
        }

        private void ProcessGoogleApiResults(string rawData, string googleApiCommand)
        {
            var rawDataObj = JToken.Parse(rawData);

            if (rawDataObj != null)
            {
                var resultCol = rawDataObj["results"];
                foreach (var result in resultCol)
                {
                    var x = new CompanyLocationOutput(result);
                    if (!_companyCol.ContainsKey(x.PlaceId))
                        _companyCol.Add(x.PlaceId, x);
                    else
                    {
                        ++_repeatCounter;
                    }
                }

                var nextPageToken = rawDataObj["next_page_token"];
                if (nextPageToken != null)
                {
                    var index = googleApiCommand.IndexOf("&pagetoken=", 0, StringComparison.Ordinal);
                    var nextPageApiUrl = (index > -1)
                        ? googleApiCommand.Substring(0, index).Trim() + "&pagetoken=" + nextPageToken.Value<string>()
                        : googleApiCommand + "&pagetoken=" + nextPageToken.Value<string>();
                    var response = ExecuteGooglePlaceApi(nextPageApiUrl);
                    
                    if(response == null)
                        throw new Exception("[Fatal Error] Failed to execute Google Place Api: " + nextPageApiUrl);

                    if (!response.ResponseHttpStatusCode.Equals(HttpStatusCode.NoContent))
                        ProcessGoogleApiResults(response.ReturnMessage, nextPageApiUrl);
                }         
            }
        }

        private HttpRequestResponse ExecuteGooglePlaceApi(string apiUrl)
        {
            var retry = false;
            HttpRequestResponse response;

            do
            {
                response = Utilities.GetHttpResponse(apiUrl,
                    null,
                    HttpApiMethods.GET,
                    string.Empty,
                    HttpcontentTypes.TextHtml,
                    true);

                var apiResponse = JToken.Parse(response.ReturnMessage);

                if (apiResponse["status"] != null)
                {
                    if (string.Compare(apiResponse["status"].Value<string>(), "ZERO_RESULTS",
                            StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        response.ResponseHttpStatusCode = HttpStatusCode.NoContent;
                        retry = false;
                    }
                    else if (string.Compare(apiResponse["status"].Value<string>(), "ok",
                            StringComparison.OrdinalIgnoreCase) != 0)
                    {
                        ++_repeatCounter2;
                        retry = true;
                        _errorBuilder.AppendLine(apiUrl);
                        _errorBuilder.AppendLine(apiResponse["status"].Value<string>());
                        System.Threading.Thread.Sleep(_apicallReAttemptTimeoutInMs);
                    }
                    else
                    {
                        retry = false;
                    }
                }
                else
                {
                    ++_repeatCounter2;
                    retry = true;
                    System.Threading.Thread.Sleep(_apicallReAttemptTimeoutInMs);
                }

            } while ((_repeatCounter2 < _apicallReAttemptCount) && retry);

            _repeatCounter2 = 0;

            return retry? null : response;
        }

        internal JObject RetrieveCompanyInfoInJson()
        {
            var x =
                new JObject(
                    new JProperty("Count", _companyCol.Count),
                    new JProperty("Companies",
                        new JArray(
                            from p in _companyCol.Values
                            select new JObject(
                                new JProperty("PlaceId", p.PlaceId),
                                new JProperty("Name", p.Name),
                                new JProperty("FormattedAddress", p.FormattedAddress),
                                new JProperty("Lat", p.Lat),
                                new JProperty("Long", p.Long)
                            )
                        )
                    )
                );

            return x;
        }
    }
}