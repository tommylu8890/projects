﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace CompanyLocationWebApi.Models
{
    public class Output
    {
        [JsonProperty("PlaceId")]
        public string PlaceId { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("FormattedAddress")]
        public string FormattedAddress { get; set; }

        [JsonProperty("Lat")]
        public string Lat { get; set; }

        [JsonProperty("Long")]
        public string Long { get; set; }

        public Output()
        {
        }
    }
}