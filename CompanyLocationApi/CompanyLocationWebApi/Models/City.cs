﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace CompanyLocationWebApi.Models
{
    internal class City
    {
        [JsonProperty("Id")]
        internal string Id { get; set; }

        [JsonProperty("CountryName")]
        internal string CountryName { get; set; }

        [JsonProperty("CityName_Ascii")]
        internal string CityNameAscii { get; set; }

        [JsonProperty("CityName_Raw")]
        internal string CityNameRaw { get; set; }

        [JsonProperty("Geo_Lat")]
        internal double GeoLat { get; set; }

        [JsonProperty("Geo_Ing")]
        internal double GeoIng { get; set; }
        
        
    }
}