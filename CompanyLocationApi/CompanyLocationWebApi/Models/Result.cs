﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CompanyLocationWebApi.Models
{
    internal class Result
    {
        [JsonProperty("Count")]
        internal int Count { get; set; }

        [JsonProperty("Companies")]
        internal List<Output> Companies { get; set; }

        internal Result()
        {
        }
    }
}