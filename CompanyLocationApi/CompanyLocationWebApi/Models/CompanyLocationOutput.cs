﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CompanyLocationWebApi.Models
{
    public class CompanyLocationOutput
    {
        [JsonProperty("PlaceId")]
        public string PlaceId { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("FormattedAddress")]
        public string FormattedAddress { get; set; }

        [JsonProperty("Lat")]
        public string Lat { get; set; }

        [JsonProperty("Long")]
        public string Long { get; set; }

        public CompanyLocationOutput(JToken result)
        {
            if (result.SelectToken("place_id") != null)
            {
                PlaceId = result.SelectToken("place_id").Value<string>();
            }

            if (result.SelectToken("name") != null)
            {
                Name = result.SelectToken("name").Value<string>();
            }

            if (result.SelectToken("formatted_address") != null)
            {
                FormattedAddress = result.SelectToken("formatted_address").Value<string>();
            }

            if (result.SelectToken("geometry.location.lat") != null)
            {
                Lat = result.SelectToken("geometry.location.lat").ToString();
            }

            if (result.SelectToken("geometry.location.lng") != null)
            {
                Long = result.SelectToken("geometry.location.lng").ToString();
            }
        }
    }
}