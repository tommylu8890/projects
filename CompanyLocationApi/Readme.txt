Author:  Tommy Lu (Topcode lu8890)


To launch the project, please open the following solution file with Visual Studio (deveopped using Visual Studio 2017)
  ..\CompanyLocationApi\CompanyLocationApi.sln

Launch Demo Webapp
1. Under Solution Explorer, please make sure "CompanyLocationWebApi" is set as the default project.
2. Open CompanyLocationWebApi\Web.config and update field key value "GoogleApiKey" with an appropriate valid Google Api Key in order to call Google Place API.
3. Click "IIS Express" to launch/start the webapp.
4. http://localhost:44418/Demo.aspx is the demo web app to test the API.
	4.1 Enter Company Name, and select Country and/or City from the dropdown, and click "Run Search" button to execute the query.
	4.2 For better performance, please use both Country and City drop downs.  Other wise, performance can be really bad.


Run API directly:
1. Please follow "Launch Demo Webapp" instruction to start the app.
2. In the browser URL:
	2.1 Replace Demo.aspx with "api/CompanyNameApi/{companyName}/{city}/{country}" to search company with city and country.
		2.1.1 Example:  http://localhost:44418/api/CompanyNameApi/walmart/lynnwood/United States}
	2.2 Replace Demo.aspx with "api/CompanyNameApi/{companyName}/city/{city}" to search company with city.
		2.1.1 Example:  http://localhost:44418/api/CompanyNameApi/walmart/city/lynnwood/}
	2.3 Replace Demo.aspx with "api/CompanyNameApi/{companyName}/country/{country}" to search company with country.
		2.1.1 Example:  http://localhost:44418/api/CompanyNameApi/walmart/country/United States}
	2.4 Replace Demo.aspx with "api/CompanyNameApi/{companyName}" to search company world wide
		2.1.1 Example:  http://localhost:44418/api/CompanyNameApi/walmart}
	2.5 Please keep in mind that provide both country and city will give the best performance.


Swagger API Documentation
Swagger is also added in the web api project, and basic api document can be find by http://localhost:44418/swagger/ui/index

Notes and Bottlenecks
1. Please refer to Utilities\SimpleMapsCsvParser. worldcities.csv was downloaded from https://simplemaps.com/data/world-cities as the datastore to retrieve cities of the world, that provides Geo coordinates for each city.  the csv file is parsed into cities.json to be consumed by the CompanyNameApi project.
2. Google Place API seems to be inconsistent in results between searchbytext and searchnearby.
  2.1 searchbytext provides a more complete data fields that will satisfy the output requiements per spec.  However, a few cons:
	2.1.1	It always have to output data.  Event when search result should have been 0, but it still would output results that is not within Geo location provided.
	2.1.2	Related to 2.1.1, not only it refuse to provide 0 results, it actually always output more outputs than it should have, which would have become one of the performance bottleneck.
  2.2 searchnearby has the advantage of willing to provide ZERO_RESULTS as appropriate.  However
	2.2.1	Results provided doesn't contain as completed info as "searchbytext" would, that means in order to get missing info, a separate call to searchbytext is required, which is a performance bottleneck.
	2.2.2	Similiar to "SearchByText", result sets return also contains items that are not related to the searching keyword.
3. For better performance, to work around performance issues specified in 2, the api call will first make the call to searchnearby first to check if it returns ZERO_RESULTS.  If it does, it stops the current thread and move on to the next.  If it doesn't, it proceed to parse the result sets.
4. There seems to be latency issues by Google Place API itself.  Espcially when running the query that returns larger result sets (for example, check company against all countries, and/or world), it will frequently hit INVALID_REQUEST against next_page_token queries, and may take a few retries to eventually load tne page successfully.  To work around this, a sleep() is called in between.  However, that adds on to the performance bottlenack.


Possible Future Improvements
Due to time constraint (I was aware of this challenge, and end up join this challenge late in the game), hence, there are a lot of researches and implementations that I hope could have done better:
1. Performance.  Performance is particularily bad on searches that doesn't contain both Country and City in the query.  Things that could be done to improve:
	1.1 spend more time on Google Place API research.  Maybe there is better query options / fields..etc that I am not aware of, that could have help with performance.
	1.2 apply multi-thread and/or async model at places where is appropriate.
	1.3 Ideally the city.json should be stored in a local or cloud data store, to save I/O on a file that it is doing now.
	1.4 Caching 
2. Data set accuracy.  Per mentioned above, it seems like dataset returned from Google Place API is not as accurate as we'd expected.  Couple things that could improve this:
	2.1 spend more time on Google Place API research.  Maybe there is better query options / fields..etc that I am not aware of, that could have help with dataset retrival.
	2.2 Out-of-bad process.  Implement some logic/process/parser/filter to filter the result sets retrieved from Google Place API to get closer to where we need to be.
3. Demo UI.  I simple ran out of time on the Demo, so it looks pretty bad in anyone's standard.
4. Test case improvement.  Due to performance and dataset correctness issue on Google Place Api mentioned above, currently only limited set of test cases are implemented until performance issue can be improved.
