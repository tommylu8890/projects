﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SimpleMapsCsvParser.models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace SimpleMapsCsvParser
{
    /// <summary>
    /// worldcities.csv contains geo info of city of the world.  It is provided by simplemaps (https://simplemaps.com/data/world-cities).
    /// Please refer to license.txt for its rights.
    /// </summary>
    internal class Program
    {
        private List<CityInfo> cities = new List<CityInfo>();

        private static void Main(string[] args)
        {
            var p = new Program();
            var fi = new FileInfo(@"csv\worldcities.csv");
            if(!fi.Exists)
                throw new FileNotFoundException("Input worldcities.csv not exist");

            string csvRawContent;
            

            using (var sr = new StreamReader(fi.FullName))
            {
                csvRawContent = sr.ReadToEnd().Trim();
            }

            if (!string.IsNullOrEmpty(csvRawContent))
            {
                var isHeader = true;
               
                foreach (var line in csvRawContent.Split('\r'))
                {
                    if (!isHeader)
                    {
                        p.cities.Add(new CityInfo(line));
                    }
                    else
                    {
                        isHeader = false;
                    }  
                }

                var jsonOutput = p.CreateJsonOutput();
                var rootDir = fi.Directory;
                if (rootDir.Exists)
                    File.WriteAllText(rootDir.FullName + @"\cities.json", jsonOutput);
            }
            else
            {
                Console.WriteLine("Input file worldcities.csv is empty... Exist");
            }
        }

        private string CreateJsonOutput()
        {
            var x =
                new JArray(
                    from p in cities
                    select new JObject(
                        new JProperty("Id", p.Id),
                        new JProperty("CountryName", p.CountryName),
                        new JProperty("CityName_Ascii", p.CityNameAscii),
                        new JProperty("CityName_Raw", p.CityNameRaw),
                        new JProperty("Geo_Lat", p.GeoLat),
                        new JProperty("Geo_Ing", p.GeoIng)
                    )
                );

            return x.ToString();
        }
    }
}
