﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleMapsCsvParser.models
{
    internal class CityInfo
    {
        internal string CityNameRaw { get; set; }
        internal string CityNameAscii { get; set; }
        internal double GeoLat { get; set; }
        internal double GeoIng { get; set; }
        internal string CountryName { get; set; }
        internal string Id { get; set; }

        internal CityInfo()
        {
        }

        internal CityInfo(string cityNameRaw,
            string cityNameAscii,
            double geoLat,
            double geoIng,
            string countryName,
            string id)
        {
            if(string.IsNullOrEmpty(cityNameRaw))
                throw new ArgumentNullException(nameof(cityNameRaw));
            CityNameRaw = cityNameRaw;

            if (string.IsNullOrEmpty(cityNameAscii))
                throw new ArgumentNullException(nameof(cityNameAscii));
            CityNameAscii = cityNameAscii;

            if (string.IsNullOrEmpty(countryName))
                throw new ArgumentNullException(nameof(countryName));
            CountryName = countryName;

            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException(nameof(id));
            Id = id;

            GeoLat = geoLat;
            GeoIng = geoIng;
        }

        internal CityInfo(string rawCsvLineInput)
        {
            if(string.IsNullOrEmpty(rawCsvLineInput.Trim()))
                throw new ArgumentNullException(nameof(rawCsvLineInput));

            var cityInfo = rawCsvLineInput.Trim().Split(',');

            if (string.IsNullOrEmpty(cityInfo[0]))
                throw new NullReferenceException("City cell can not be null or empty");
            CityNameRaw = cityInfo[0].Replace("\"", string.Empty).Trim();

            if (string.IsNullOrEmpty(cityInfo[1]))
                throw new NullReferenceException("City_ascii cell ca not be null or empty");
            CityNameAscii = cityInfo[1].Replace("\"", string.Empty).Trim();

            if (string.IsNullOrEmpty(cityInfo[4]))
                throw new NullReferenceException("country cell can not be null or empty");
            CountryName = cityInfo[4].Replace("\"", string.Empty).Trim();

            if (string.IsNullOrEmpty(cityInfo[10]))
                throw new NullReferenceException("id cell can not be null or empty");
            Id = cityInfo[10].Replace("\"", string.Empty).Trim();

            double tempDbl;

            if(!double.TryParse(cityInfo[2].Replace("\"", string.Empty).Trim(), out tempDbl))
                throw new FormatException("lat cell must be typeof Double");
            GeoLat = tempDbl;

            if (!double.TryParse(cityInfo[3].Replace("\"", string.Empty).Trim(), out tempDbl))
                throw new FormatException("Ing cell must be typeof Double");
            GeoIng = tempDbl;

        }
    }
}
